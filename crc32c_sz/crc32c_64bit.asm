;
; Command line CRC32C calculation utility for Linux using SSE4.2 CRC32C instruction.
;
; Notes:
;   - Output format is "<CRC32C as hex>  <File Size>".
;   - The file size is right-aligned so that the whole message is 28 characters long.
;

; Buffer may be not TOO tiny cause output string is also written there.
%define BUFSIZE 4096

bits 64

section .text

%define sys_read 0x00
%define sys_write 0x01
%define sys_open 0x02
%define sys_close 0x03
%define sys_exit 0x3C

%define stdout 1
%define stderr 2

align 16
strlen: ; RSI should point to string, RDX will hold result.
	push rax
	push rsi
	cld
.loop:
	lodsb
	test al, al
	jnz .loop
	mov rdx, rsi
	pop rsi
	sub rdx, rsi
	pop rax
	ret

align 16
print: ; Print text, pointer in RSI.
	call strlen
	mov rdi, stdout
	mov rax, sys_write
	syscall
	ret

align 16
eprint: ; Print text to STDERR, pointer in RSI.
	call strlen
	mov rdi, stderr
	mov rax, sys_write
	syscall
	ret

align 16
eexit: ; Quit program in error.
	pop rax ; Lose our own adress.
	mov rdi, 1
	mov rax, sys_exit
	syscall
	pop rbp
	ret

%macro fail 1+
	mov rsi, %%str
	call eprint
	call eexit
	%%str: db %1, 10, 0
%endmacro

global _start
align 16
_start:
	push rbp
	mov rbp, rsp
	; Check whether SSE4.2 is supported.
	mov rax, 1
	cpuid
	and ecx, 1 << 20
	jnz .sse42_ok
	fail "error: CPU does not support SSE4.2"
.sse42_ok:
	; Check if we got the right amount of arguments (1).
	cmp qword [rbp+8], 2 ; argc - 2
	je .argc_ok
	; If we reach here try to explain how the user should do instead.
	mov rsi, .err_args1
	call eprint
	mov rsi, [rbp+16] ; argv[0]
	call eprint
	mov rsi, .err_args2
	call eprint
	call eexit
	.err_args1: db "usage: ", 0
	.err_args2: db " <filename>", 10, 0
.argc_ok:
	; Try to open the specified file.
	xor rdx, rdx ; mode
	xor rsi, rsi ; flags
	mov rdi, [rbp+24] ; argv[1]
	mov rax, sys_open
	syscall
	mov [fd], eax
	test eax, eax
	jns .open_ok
	fail "error: can't open file"
.open_ok:
	; Actual CRC calculation starts here, EBX will hold the sum.
	mov ebx, 0xFFFFFFFF
align 16
.doblock:
	; Read a bit of the file.
	push rbx
	mov rdx, BUFSIZE
	mov rsi, buf
	mov rdi, [fd]
	mov rax, sys_read
	syscall
	pop rbx
	; Check for error return.
	test rax, rax
	jns .read_ok
	fail "error: can't read file"
.read_ok:
	; Count the length while we're at it.
	add [len], rax
	; Calculate CRC of that bit.
	mov rdx, buf
.crc32c: ; RAX = length, EBX = sum, RDX = buf
	; Do 8-byte blocks.
	mov rcx, rax
	shr rcx, 3
	test rcx, rcx
	jz .end64
align 16
.step64:
	crc32 rbx, qword [rdx] ; Doesn't seem to matter much wether aligned.
	add rdx, 8
	dec rcx
	jnz .step64
.end64:
	; Finish.
	mov rcx, rax
	and rcx, 7
	test rcx, rcx
	jz .end
align 16
.step8:
	crc32 ebx, byte [rdx]
	inc rdx
	dec rcx
	jnz .step8
.end:
	cmp rax, BUFSIZE
	je .doblock
	xor ebx, 0xFFFFFFFF
	; Close the file.
	push rbx
	mov rdi, [fd]
	mov rax, sys_close
	syscall
	pop rbx
	; Convert checksum to ASCII hex form.
	mov rcx, 8
	mov rdi, buf + 7
	std
.hexloop:
	mov al, bl
	and al, 0x0F
	cmp al, 9
	jng .notletter
	add ax, 'A' - '0' - 10
.notletter:
	add ax, '0'
	stosb
	shr rbx, 4
	dec rcx
	jnz .hexloop
	; Convert file length to readable form.
	cld
	mov rdi, buf + 8
	mov al, ' '
	mov rcx, 20
	rep stosb
	mov al, 10
	stosb
	xor al, al
	stosb
	std
	sub rdi, 3
.divloop:
	; Divide the size by 10.
	mov rax, [len]
	xor rdx, rdx
	mov rbx, 10
	div rbx
	mov [len], rax
	; Now we have remainder of division by 10.
	mov al, dl
	add ax, '0'
	stosb
	; Repeat if necessary.
	cmp dword [len+4], 0
	jnz .divloop
	cmp dword [len], 0
	jnz .divloop
	; Print it all.
	mov rsi, buf
	call print
	; The end.
	xor rdi, rdi
	mov rax, sys_exit
	syscall
	pop rbp
	ret

section .bss
	fd: resd 1
	len: resq 1
	buf: resb BUFSIZE
