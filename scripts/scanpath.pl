#!/usr/bin/perl
use warnings;
use strict;

my $tmppath = "/tmp/";
my $outfile1 = $tmppath . "sp_stage1_" . $$ . ".txt";
my $outfile2 = $tmppath . "sp_stage2_" . $$ . ".txt";
my $outfile3 = $tmppath . "sp_stage3_" . $$ . ".txt";
my $crc32c = "crc32c";

# Read command line arguments.
if ($#ARGV + 1 != 2) {
	print STDERR "usage: " . $0 . " <pathname> <output file>\n";
	exit;
}
my $path = $ARGV[0];
my $dest = $ARGV[1];

# Do our thing.
list_build($outfile1, $path);
list_sort($outfile2, $outfile1);
unlink($outfile1);
list_sum($outfile3, $outfile2);
unlink($outfile2);
if ($dest ne "-") {
	system("mv " . makesafe($outfile3) . " " . makesafe($dest));
	if ($? != 0) {
		print STDERR "error: Moving failed, results file remains in " . $outfile3 . "\n";
	}
} else {
	system("cat " . makesafe($outfile3));
	unlink($outfile3);
}

# Function to make pathnames safe for use (to commandline, place no quotes around it!).
sub makesafe {
	my ($fn) = $_[0];
	$fn =~ s/([^a-zA-Z0-9\.\_\+\%\-\/])/\\$1/g;
	return $fn;
}

##############################
# Stage 1, build files list. #
##############################

sub list_build { # dst, src
	print STDERR "Making list of filenames in path... ";
	system('find ' . makesafe($_[1]) . ' -mindepth 1 -noleaf -printf "%y %13s -------- %P\n" > ' . makesafe($_[0]));
	exit if ($? != 0);
	print STDERR "Done.\n";
}

#####################
# Stage 2, sorting. #
#####################

sub list_sort { # dst, src
	print STDERR "Sorting the list...\n";

	# Open the file we created.
	open my $fda, $_[1] or die "Could not open " . $_[1] . ": $!";

	# Figure out how many lines it has (for showing progress).
	while (<$fda>) {};
	my $lines = $.;
	seek $fda, 0, 0;
	print STDERR "  " . $lines . " files to process.\n";

	# Read the list to a hashmap.
	print STDERR "  Building file tree... ";
	open my $fdb, '>' . $_[0] or die "Could not open " . $_[0] . ": $!";
	my %map;
	while (my $line = <$fda>) {
		# Ignore comments and make sure isn't empty line.
		$line =~ s/^\s+//;
		chomp $line;
		next if ($line eq "" || $line =~ m/^\#/);
		# Add it to list.
		$line =~ m/^(.)\s(\s*[0-9]+)\s+([0-9a-fA-F\-]+)\s+(.*)$/;
		my ($t) = $1;
		my ($s) = $2;
		my ($c) = $3;
		my ($p) = $4;
		my @path = split /\//, $p;
		my $ptr = \%map;
		foreach my $f (@path) {
			if (!defined ${$ptr}{$f}) {
				$ptr->{$f} = { t => $t, s => $s, c => $c, sub => {} };
			} else {
				die "error: Subfolders in non-folder item!\n" if ($ptr->{$f}->{t} ne "d");
			}
			$ptr = $ptr->{$f}->{sub};
		}
	}
	print STDERR "Done.\n";
	# Reconstruct this shit while sorting.
	print STDERR "  Sorting and writing out... ";
	my (@stk_ptr, @stk_fn, @stk_pos);
	my $ptr = \%map;
	my @keys = sort keys %{$ptr};
	my $pos = 0;
	while (1) {
		my $f = $keys[$pos];
		if (!defined $f) {
			last if (!@stk_ptr);
			$ptr = pop @stk_ptr;
			$pos = pop @stk_pos;
			pop @stk_fn;
			@keys = sort keys %{$ptr};
			++$pos;
			next;
		}
		print $fdb $ptr->{$f}->{t} . " " . $ptr->{$f}->{s} . " " . $ptr->{$f}->{c} . " " . join('/', @stk_fn, $f) . "\n";
		if (defined $ptr->{$f}->{t} && $ptr->{$f}->{t} eq "d") {
			push @stk_ptr, $ptr;
			push @stk_pos, $pos;
			push @stk_fn, $f;
			$ptr = $ptr->{$f}->{sub};
			@keys = sort keys %{$ptr};
			$pos = 0;
			next;
		}
		++$pos;
	}
	close $fda;
	close $fdb;
	print STDERR "Done.\n";
}

###############################
# Stage 3, collect checksums. #
###############################

sub list_sum { # dst, src
	print STDERR "Calculating checksums...\n";

	# Open the file we created.
	open my $fda, $_[1] or die "Could not open " . $_[1] . ": $!";

	# Figure out how many lines it has (for showing progress).
	while (<$fda>) {};
	my $lines = $.;
	seek $fda, 0, 0;
	print STDERR "  " . $lines . " items to process.\n";

	# Write a copy with CRCs filled in.
	open my $fdb, '>' . $_[0] or die "Could not open " . $_[0] . ": $!";
	my $current = 0;
	my $numfiles = 0;
	my $numbytes = 0;
	while (my $line = <$fda>) {
		# Ignore comments and make sure isn't empty line.
		$line =~ s/^\s+//;
		chomp $line;
		next if ($line eq "" || $line =~ m/^\#/);
		# Begin actual work.
		$line =~ m/^(.)\s+([0-9]+)\s+[0-9a-fA-F\-]+\s+(.*)$/;
		if ($1 eq "d") { # Skip directories.
			# Make directories 0 size for easier comparison.
			$line =~ s/^(.\s)\s*[0-9]+(\s+[0-9a-fA-F\-]+\s+.*)$/$1            0$2/;
			print $fdb $line . "\n";
			next;
		}
		if ($1 ne "f") { # Skip special files.
			# Make special files 0 size for easier comparison.
			$line =~ s/^(.\s)\s*[0-9]+(\s+[0-9a-fA-F\-]+\s+.*)$/$1            0$2/;
			print $fdb $line . "\n";
			print STDERR "\e[2K  Skipping special file: " . $2 . "\n"; # TODO this is not right
			next;
		}
		++$numfiles;
		$numbytes += $2;
		# Show what file this is.
		chomp $3;
		(my $dn = $3) =~ s/^.*\///;
		print STDERR "\e[2K  " . $current . "/" . $lines . " " . $dn . "\r";
		$current++;
		# Get CRC of it.
		$line =~ m/^(.)\s+[0-9]+\s+[0-9a-fA-F\-]+\s+(.*)$/;
		my $sfn = makesafe($path . '/' . $2);
		my ($sum) = `$crc32c $sfn 2> /dev/null`;
		if (!defined $sum) {
			print $fdb $line . "\n";
			print STDERR "\e[2K  Error creating checksum for: " . $2 . "\n";
			next;
		}
		chomp $sum;
		$line =~ s/^(.\s+[0-9]+\s+)[0-9a-fA-F\-]+(\s+.*)$/$1$sum$2/;
		print $fdb $line . "\n";
	}
	close $fda;
	close $fdb;
	print STDERR "\e[2K  Done.\n";
	print STDERR "Processed $numbytes bytes in $numfiles files.\n";
}
