#!/usr/bin/perl
use warnings;
use strict;

my $tmppath = "/tmp/";
my $outfile1 = $tmppath . "fd_stage1_" . $$ . ".txt";

# Read command line arguments.
if ($#ARGV + 1 != 1) {
	print STDERR "usage: " . $0 . " <source file>\n";
	exit;
}
my $path = $ARGV[0];

# Do our thing.
list_countbytes($outfile1, $path);

# Function to make pathnames safe for use (to commandline, place no quotes around it!).
sub makesafe {
	my ($fn) = $_[0];
	$fn =~ s/([^a-zA-Z0-9\.\_\+\%\-\/])/\\$1/g;
	return $fn;
}

sub list_countbytes { # dst, src
	print STDERR "Counting file sizes...\n";

	# Open the file we created.
	open my $fda, $_[1] or die "Could not open " . $_[1] . ": $!";

	# Figure out how many lines it has (for showing progress).
	while (<$fda>) {};
	my $lines = $.;
	seek $fda, 0, 0;
	print STDERR "  " . $lines . " items to process.\n";

	my $files = 0;
	my $size = 0;

	# Count the bytes.
	while (my $line = <$fda>) {
		# Ignore comments and make sure isn't empty line.
		$line =~ s/^\s+//;
		chomp $line;
		next if ($line eq "" || $line =~ m/^\#/);
		# Count.
		$line =~ m/^(.)\s+([0-9]+)\s+[0-9a-fA-F\-]+\s+.*$/;
		next if ($1 ne "f"); # Skip directories and special files.
		++$files;
		$size += $2;
	}

	close $fda;
	print STDERR "  Done.\n";
	print "$size bytes in $files files.\n";
}
