#!/usr/bin/perl
use warnings;
use strict;

#
# This repeats the making checksums part of scanpath.pl
# You can use it to verify whether a folder with the files from scanpath.pl that also holds other files
#   holds copies with the right checksum. (by checking diff between input and output file)
#

my $tmppath = "/tmp/";
my $outfile1 = $tmppath . "fd_stage1_" . $$ . ".txt";
my $crc32c = "crc32c";

# Read command line arguments.
if ($#ARGV + 1 != 3) {
	print STDERR "usage: " . $0 . " <source file> <source path> <output file>\n";
	exit;
}

my $input = $ARGV[0];
my $path = $ARGV[1];
my $dest = $ARGV[2];

# Do our thing.
list_sum($outfile1, $input);
if ($dest ne "-") {
	system("mv " . makesafe($outfile1) . " " . makesafe($dest));
	if ($? != 0) {
		print STDERR "error: Moving failed, results file remains in " . $outfile1 . "\n";
	}
} else {
	system("cat " . makesafe($outfile1));
	unlink($outfile1);
}

# Function to make pathnames safe for use (to commandline, place no quotes around it!).
sub makesafe {
	my ($fn) = $_[0];
	$fn =~ s/([^a-zA-Z0-9\.\_\+\%\-\/])/\\$1/g;
	return $fn;
}

sub list_sum { # dst, src
	print STDERR "Calculating checksums...\n";

	# Open the file we created.
	open my $fda, $_[1] or die "Could not open " . $_[1] . ": $!";

	# Figure out how many lines it has (for showing progress).
	while (<$fda>) {};
	my $lines = $.;
	seek $fda, 0, 0;
	print STDERR "  " . $lines . " items to process.\n";

	# Write a copy with CRCs filled in.
	open my $fdb, '>' . $_[0] or die "Could not open " . $_[0] . ": $!";
	my $current = 0;
	my $numfiles = 0;
	my $numbytes = 0;
	while (my $line = <$fda>) {
		# Ignore comments and make sure isn't empty line.
		$line =~ s/^\s+//;
		chomp $line;
		next if ($line eq "" || $line =~ m/^\#/);
		# Begin actual work.
		$line =~ m/^(.)\s+([0-9]+)\s+[0-9a-fA-F\-]+\s+(.*)$/;
		if ($1 eq "d") { # Skip directories.
			# Make directories 0 size for easier comparison.
			$line =~ s/^(.\s)\s*[0-9]+(\s+[0-9a-fA-F\-]+\s+.*)$/$1            0$2/;
			print $fdb $line . "\n";
			next;
		}
		if ($1 ne "f") { # Skip special files.
			# Make special files 0 size for easier comparison.
			$line =~ s/^(.\s)\s*[0-9]+(\s+[0-9a-fA-F\-]+\s+.*)$/$1            0$2/;
			print $fdb $line . "\n";
			print STDERR "\e[2K  Skipping special file: " . $2 . "\n";
			next;
		}
		++$numfiles;
		$numbytes += $2;
		# Show what file this is.
		chomp $3;
		(my $dn = $3) =~ s/^.*\///;
		print STDERR "\e[2K  " . $current . "/" . $lines . " " . $dn . "\r";
		$current++;
		# Get CRC of it.
		$line =~ m/^(.)\s+[0-9]+\s+[0-9a-fA-F\-]+\s+(.*)$/;
		my $sfn = makesafe($path . '/' . $2);
		my ($sum) = `$crc32c $sfn 2> /dev/null`;
		if (!defined $sum) {
			print $fdb $line . "\n";
			print STDERR "\e[2K  Error creating checksum for: " . $2 . "\n";
			next;
		}
		chomp $sum;
		$line =~ s/^(.\s+[0-9]+\s+)[0-9a-fA-F\-]+(\s+.*)$/$1$sum$2/;
		print $fdb $line . "\n";
	}
	close $fda;
	close $fdb;
	print STDERR "\e[2K  Done.\n";
	print STDERR "Processed $numbytes bytes in $numfiles files.\n";
}
