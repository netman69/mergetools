#!/usr/bin/perl
use warnings;
use strict;

my $tmppath = "/tmp/";
my $outfile1 = $tmppath . "fd_stage1_" . $$ . ".txt";

# Read command line arguments.
if ($#ARGV + 1 != 2) {
	print STDERR "usage: " . $0 . " <source file> <output file>\n";
	exit;
}
my $path = $ARGV[0];
my $dest = $ARGV[1];

# Do our thing.
list_dups($outfile1, $path);
if ($dest ne "-") {
	system("mv " . makesafe($outfile1) . " " . makesafe($dest));
	if ($? != 0) {
		print STDERR "error: Moving failed, results file remains in " . $outfile1 . "\n";
	}
} else {
	system("cat " . makesafe($outfile1));
	unlink($outfile1);
}

# Function to make pathnames safe for use (to commandline, place no quotes around it!).
sub makesafe {
	my ($fn) = $_[0];
	$fn =~ s/([^a-zA-Z0-9\.\_\+\%\-\/])/\\$1/g;
	return $fn;
}

sub list_dups { # dst, src
	print STDERR "WARNING! This utility only checks sums and size!\nSearching for duplicate files...\n";

	# Open the file we created.
	open my $fda, $_[1] or die "Could not open " . $_[1] . ": $!";

	# Figure out how many lines it has (for showing progress).
	while (<$fda>) {};
	my $lines = $.;
	seek $fda, 0, 0;
	print STDERR "  " . $lines . " items to process.\n";
	
	my %map;
	my @map_order;

	# Add items to a map so we can see the duplicates easy.
	open my $fdb, '>' . $_[0] or die "Could not open " . $_[0] . ": $!";
	while (my $line = <$fda>) {
		# Ignore comments and make sure isn't empty line.
		$line =~ s/^\s+//;
		chomp $line;
		next if ($line eq "" || $line =~ m/^\#/);
		# Add to map.
		$line =~ m/^(.)\s+([0-9]+\s+[0-9a-fA-F\-]+)\s+.*$/;
		next if ($1 ne "f"); # Skip directories and special files.
		# Add this item to the hashmap.
		my $idx = $2;
		if (!defined $map{$idx}) {
			$map{$idx} = [];
			push @map_order, $idx;
		}
		push @{$map{$idx}}, $line;
	}
	# Print out the items that appear to be duplicates.
	my $excess = 0;
	foreach my $idx (@map_order) {
		my @arr = @{$map{$idx}};
		next if ($#arr + 1 < 2);
		my $s;
		foreach my $line (@arr) {
			print $fdb $line . "\n";
			$line =~ m/^.\s+([0-9]+)\s+[0-9a-fA-F\-]+\s+.*$/;
			$excess += $1;
			$s = $1;
		}
		$excess -= $s;
	}

	close $fda;
	close $fdb;
	print STDERR "  Done.\n";
	print STDERR $excess . " bytes in duplicate files.\n";
}
